create database Product
use Product
create table product_tbl
(
    Id       int          not null
        primary key,
    name     varchar(255) null,
    quantity int          null,
    price    int          null
);